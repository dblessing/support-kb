### Support Team Knowledge Base

Welcome to the support team knowledge base, here you'll find various resources 
for troubleshooting GitLab technical issues and applying workflows in ZenDesk.

Anyone can suggest changes to these documents. Submit a merge request and 
mention the team lead and/or senior engineers. Additionally, add the change
to the next Support team call agenda so everyone is aware of the change.

#### Support Workflows

+ [Dormant Username](workflows/Workflow-Dormant-Username.md)
+ [Twitter Mentions](workflows/Workflow-Twitter-Mentions.md)
+ [2FA Removal - GitLab.com](workflows/Workflow-2FA-Removal.md)

#### Troubleshooting

+ [Diagnose Errors on GitLab.com](troubleshooting/Diagnose-Errors-GitLab.com.md)
+ [Debugging LDAP](troubleshooting/Debugging-LDAP.md)

#### Shared Infrastructure

+ [Jira Server](shared-infrastructure/jira.html.md)
+ [Jenkins Server](shared-infrastructure/jenkins.html.md)

#### Creating new workflows

Use the workflow template
 + [Workflow Template](workflows/Workflow-Template.md)

 ______________________

Support team project: https://gitlab.com/gitlab-com/support
