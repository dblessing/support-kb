### Debugging LDAP

##### Notes:

This assumes an omnibus installation.

______________


See LDAP troubleshooting in docs - [View Docs](http://docs.gitlab.com/ee/administration/auth/ldap.html#troubleshooting)

**Removing exclusive lease** - Testing (valid for 8.6 to 8.9)

This is used to force an instant sync of LDAP for testing purposes. 

1. Edit any LDAP settings required
1. Edit `vi /opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/ldap/group_sync.rb`
1. Comment out the exclusive lease section *(lines may differ in releases)* - [View code](https://gitlab.com/gitlab-org/gitlab-ee/blob/5c8b211c7b8746ec6d5697e495ddb68f2ac08dd7/lib/gitlab/ldap/group_sync.rb#L69-74) 
1. Run a reconfigure `sudo gitlab-ctl reconfigure` **This will restart GitLab**
1. Launch GitLab rails console `gitlab-rails console`
1. Execute `Gitlab::LDAP::GroupSync.execute`
1. LDAP sync will now run
1. **Revert changes to the `group_sync.rb` file when finished**
 `/opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/ldap/group_sync.rb`

**Additional testing**

1. Start the rails console

```
sudo gitlab-rails console
```
2. Create a new adapter instance

```
adapter = Gitlab::LDAP::Adapter.new('ldapmain')
```
   
3. Find a group by common name. Replace **UsersLDAPGroup** with the common name to search.

```
group =  Gitlab::LDAP::Group.find_by_cn('UsersLDAPGroup', adapter)
```
   
4. Check `member_dns`

```
group.member_dns
```